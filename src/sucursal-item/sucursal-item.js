import { LitElement, html, css } from 'lit-element';
export class SucursalItem extends LitElement {
  static get properties() {
    return {
        sucursalNombre: {type: String},
        sucursalDireccion: {type: String},
        sucursalEstado: {type: Boolean},
    };
  }
  constructor() {
    super();
  }
  static get styles() {
    return css`
      
      .rounded-circle{
        width: 15px;
        height: 10px;
        margin-left: 0px;
      }
      .rect-circle{
        width: 100%;
        height: 100%;
      }
      .free{
        fill: #28a745;
      }
      .full{
        fill: #dc3545;
      }
      .bg-free{
        background: #eaf8e3;
      }

    `;
  }

  render() {
    return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <li class="list-group-item d-flex justify-content-between lh-condensed ">
            <div class="text-muted">
                <h6 class="my-0">${this.sucursalNombre}</h6>
                <small class="text-muted"><strong>Dirección: </strong>${this.sucursalDireccion}</small>
            </div>
            <svg class="rounded-circle"><rect class="rect-circle full"></rect></svg>
        </li>
    `;
  }
}
customElements.define('sucursal-item', SucursalItem)

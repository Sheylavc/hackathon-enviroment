import { LitElement, html, css } from 'lit-element';
import "lit-google-map/dist/lit-google-map.bundle.js";
import "../sucursal-item/sucursal-item.js";
export class ProjectContainer extends LitElement {
  static get properties() {
    return {
        sucursales: {type: Array},
    };
  }
  constructor() {
    super();
    this.sucursales = [
        {
            "nombre": "Oficina 1" , 
            "direccion": "CRUCE AV. TOMAS VALLE CON PANAMERICANA NORTE, LOCAL CF-06",
            "estado" : true
        },
        {
            "nombre": "Oficina 1" , 
            "direccion": "CRUCE AV. TOMAS VALLE CON PANAMERICANA NORTE, LOCAL CF-06",
            "estado" : true
        }
    ]
  }
  static get styles() {
    return css`
      .container {
        width: 100%;
        margin-right: auto;
        margin-left: auto;
      }
      .row{
        padding: 0px;
        margin-left: 0px;
        
      }
      .map-heading{
        background: #004481;
        margin-bottom: 0;
        padding: 10px;
        font-size: 20px;
        border-radius: 3px 3px 0 0;
        color: white;
        margin-top: 50px !important;
        color: white;
        text-align: center;
      }
      .map{
        display: block;
        height: 600px;
        
      }
      .leftblock{
        padding-right: 0px;
      }
      .rightblock{
        padding-left: 0px;
      }
      
      .rounded-circle{
        width: 10px;
        height: 10px;
        margin-left: 20px;
      }
      .rect-circle{
        width: 100%;
        height: 100%;
      }

      .info{
        background: #edeff2;
      }
      .free{
        fill: #28a745;
      }
      .full{
        fill: #dc3545;
      }
    `;
  }

  render() {
    return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <div class="container">
            <div class="row">
                <div class="col-md-12 order-md-1">
                    <h3 class="map-heading">Nuestras Oficinas</h3>
                </div>
                
                <div class="col-md-8 order-md-1 leftblock">
                    <div class="map">
                        <lit-google-map 
                            class="map"
                            api-key="AIzaSyDLUdFsB0aDi9paOaigBBl6YbByijjY3sM" 
                            version="3.43" zoom="13" 
                            center-latitude="-12.0824865" 
                            center-longitude="-77.0360425">

                                    <lit-google-map-marker slot="markers" latitude="-12.0824863" longitude="-77.0262691">
                                    <p>oficina 1</p>
                                    </lit-google-map-marker>
                        </lit-google-map>
                    </div>
                </div>
                <div class="col-md-4 order-md-2 rightblock">

                    <ul class="list-group">
                        
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <small><strong>Disponibilidad </strong></small>
                            <div>
                                <svg class="rounded-circle"><rect class="rect-circle full"></rect></svg>
                                <small class="text-muted">Aforo Lleno</small>
                            </div>
                            <div>
                                <svg class="rounded-circle"><rect class="rect-circle free"></rect></svg>
                                <small class="text-muted">Aforo Disponible</small>
                            </div>
                        </li>
                        ${this.sucursales.map(
                            sucursal => html`<sucursal-item sucursalNombre="${sucursal.nombre}" sucursalDireccion="${sucursal.direccion}" sucursalEstado="${sucursal.estado}"></sucursal-item>`
                        )}

                        <!--li class="list-group-item d-flex justify-content-between bg-free">
                        <div >
                            <h6 class="my-0">Oficina 2</h6>
                            <small><strong>Dirección: </strong>Brief description</small>
                        </div>
                        <svg class="rounded-circle"><rect class="rect-circle free"></rect></svg>
                        </li>

                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div class="text-muted">
                            <h6 class="my-0">Oficina 3</h6>
                            <small class="text-muted"><strong>Dirección: </strong>Brief description</small>
                        </div>
                        <svg class="rounded-circle"><rect class="rect-circle full"></rect></svg>
                        </li>

                        <li class="list-group-item d-flex justify-content-between bg-free">
                        <div >
                            <h6 class="my-0">Oficina 4</h6>
                            <small><strong>Dirección: </strong>Brief description</small>
                        </div>
                        <svg class="rounded-circle"><rect class="rect-circle free"></rect></svg>
                        </li-->

                    </ul>
                </div>
                
            </div>
            
        </div>
      
    
    `;
  }
}
customElements.define('project-container', ProjectContainer)
import { LitElement, html, css } from 'lit-element';
import "../project-header/project-header.js";
import "../project-container/project-container.js";
export class ProjectMain extends LitElement {
  static get properties() {
    return {
      
    };
  }
  constructor() {
    super();
  }

  render() {
    return html`
      <project-header></project-header>
      
      <project-container></project-container>
    
    `;
  }
}
customElements.define('project-main', ProjectMain)

import { LitElement, html, css } from 'lit-element';
export class ProjectHeader extends LitElement {
  static get properties() {
    return {
      
    };
  }
  constructor() {
    super();
  }

  static get styles() {
    return css`
      .cs-header{
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 30px;
        min-height: 50px;
        color: #fff;
        background: #072146;
      }
      .header_image{
        height: 20px;
        width: auto;
      }
    `;
  }
  render() {
    return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <header class="cs-header">
          
          <img class="header_image" src="./src/resources/bbvalogo.png"/>
        </header>
      
    
    `;
  }
}
customElements.define('project-header', ProjectHeader)
